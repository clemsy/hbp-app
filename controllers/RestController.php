<?php

namespace app\controllers;
use Yii;
use app\models\Participant;
use yii\web\Response;
use yii\helpers\BaseJson;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class RestController extends \yii\rest\Controller
{


    private $format = 'json';

    public $enableCsrfValidation = false;


    public static function allowedDomains() {
        return [
            '*',
            'http://hbptm.clm',
            'https://hbptm.clm',
            'http://app.eurobikepolo.com/',
            'https://app.eurobikepolo.com/',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => []
                ],

            ],
        ];
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    public function actionClubs()
    {
        header('Access-Control-Allow-Origin: *');
        echo json_encode([
            "markers" =>  [
                ["lat"=>-37.8210922667, "lng"=>175.2209316333, "popup" => "2"],
                ["lat"=>-37.8210819833, "lng"=>175.2213903167, "popup" => "3"],

            ]
        ]);
    }

    public function actionJson()
    {
        header('Access-Control-Allow-Origin: *');
        $query = (new \yii\db\Query())
            ->select([
                'participants.user_id',
                'participants.firstname',
                'participants.nickname',
                'participants.pronoum',
                'SUBSTRING(participants.lastname, 1, 1) AS lastname',
                'CONCAT(firstname, " <span class=\'small\'> ", IF(pronoum=2, "(they)", IF(pronoum=1, "(she)", IF(pronoum=3, "(he)", ""))),"</span>  <span class=\'nickname\'>", nickname, "</span> ", SUBSTRING(participants.lastname, 1, 1),".") AS name',
                'CONCAT(firstname, " ", SUBSTRING(participants.lastname, 1, 1),".") AS cleanname',
                'participants.city',
                'participants.company',
                'participants.race_number AS seat',
                'IF(participants.category=2, "wtnb", "open") AS category',
                'participants.profile_picture',
                'country.alpha as country_code',
                'country.name_en as country',
                // 'participants.message as message',
                'participants.registration_is_paid as is_paid',
                'participants.registration_is_valid as is_valid',
            ])
            ->from('participants')
            ->innerJoin('country', 'participants.country = country.id')
            ->innerJoin('user', 'participants.user_id = user.id')
            ->where(['is_active' => 1, 'is_admin' => null])
            ->orderBy('user_id DESC');


        return $query->all();
    }

    public function actionStats($size=null, $cat=null, $race_cat=null, $payment=null, $diet=null)
    {
        header('Access-Control-Allow-Origin: *');
        $query = (new \yii\db\Query())
            ->select([
                'participants.user_id',
                'participants.race_number AS seat',
                'CONCAT(firstname, " <span class=\'small\'> ", IF(pronoum=2, "(they)", IF(pronoum=1, "(she)", IF(pronoum=3, "(he)", ""))),"</span>  <span class=\'nickname\'>", nickname, "</span> ", SUBSTRING(participants.lastname, 1, 1),".") AS name',
                'CONCAT(firstname, " ", participants.lastname) AS fullname',

                //'IF(participants.category=2, "wtnb", "open") AS category',
                'participants.category AS category',
                'participants.race_category AS race_category',
                'participants.teeshirt_size AS teeshirt_size',
                'participants.diet AS diet',
                'participants.profile_picture',
                'country.alpha as country_code',
                'country.name_en as country',
                'participants.registration_is_paid as is_paid',
                'participants.registration_is_valid as is_valid',
                //'participants.registration_payment_date as payment_date',
                'DATE_FORMAT(participants.registration_payment_date, "%M") as payment_date',
                'participants.registration_payment_date as registration_payment_date',
                'participants.registration_payment_medium as payment_medium',
            ])
            ->from('participants')
            ->innerJoin('country', 'participants.country = country.id')
            ->innerJoin('user', 'participants.user_id = user.id')
            ->where(['is_active' => 1, 'is_admin' => null])
            ->orderBy('user_id DESC');

        if($size) {
            $query->andWhere('teeshirt_size='.$size);
        }
        if($cat) {
            $query->andWhere('category='.$cat);
        }
        if($race_cat) {
            $query->andWhere('race_category='.$race_cat);
        }
        if($payment) {
            if($payment=='null') {
                $query->andWhere('participants.registration_is_paid IS NULL');
            }else{
                $query->andWhere('registration_is_paid='.$payment);
            }
        }
        if($diet) {
            $query->andWhere('diet='.$diet);
        }

        return $query->all();
    }

    public function actionMessages()
    {

        header('Access-Control-Allow-Origin: *');
        $query = (new \yii\db\Query())
            ->select([
                'participants.user_id',
                'participants.race_number AS seat',
                'participants.firstname',
                'participants.nickname',
                'participants.pronoum',
                'SUBSTRING(participants.lastname, 1, 1) AS lastname',
                'CONCAT(firstname, " <span class=\'small\'> ", IF(pronoum=2, "(they)", IF(pronoum=1, "(she)", IF(pronoum=3, "(he)", ""))),"</span>  <span class=\'nickname\'>", nickname, "</span> ", SUBSTRING(participants.lastname, 1, 1),".") AS name',
                'participants.profile_picture',
                'participants.message'
            ])
            ->from('participants')
            ->innerJoin('user', 'participants.user_id = user.id')
            ->where(['IS NOT', 'message', null])
            ->andWhere(['!=', 'message', ""])
            ->andWhere(['=', 'is_active', 1]);
        return $query->all();
    }

    public function actionProfiles()
    {

        header('Access-Control-Allow-Origin: *');
        $query = (new \yii\db\Query())
            ->select([
                'participants.user_id',
                'participants.firstname',
                'participants.nickname',
                'participants.pronoum',
                'SUBSTRING(participants.lastname, 1, 1) AS lastname',
                'CONCAT(firstname, " <span class=\'small\'> ", IF(pronoum=2, "(they)", IF(pronoum=1, "(she)", IF(pronoum=3, "(he)", ""))),"</span>  <span class=\'nickname\'>", nickname, "</span> ", SUBSTRING(participants.lastname, 1, 1),".") AS name',
                'CONCAT(firstname, " ", SUBSTRING(participants.lastname, 1, 1),".") AS cleanname',
                'participants.city',
                'participants.company',
                'participants.race_number AS seat',
                'IF(participants.category=2, "wtnb", "open") AS category',
                'participants.profile_picture',
                'country.alpha as country_code',
                'country.name_en as country',
                'participants.message as message'
            ])
            ->from('participants')
            ->innerJoin('country', 'participants.country = country.id')
            ->innerJoin('user', 'participants.user_id = user.id')
            ->where(['is_active' => 1, 'is_admin' => null])
            ->orderBy('user_id DESC');


        return $query->all();
    }
}
