<?php

namespace app\controllers;

use app\models\Tournament;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\User;
use app\models\Club;
use yii\data\ActiveDataProvider;
//use app\widgets\customAlert;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $clubs = Club::listClubsByCountry()->all();

        $agendaDataProvider = new ActiveDataProvider([
            'query' => Tournament::find()
                ->select( ['DATE_FORMAT(date_from, "%M %Y") AS m_date', 'date_from', 'date_to', 'name', 'slug', 'team_max', 'city', 'country', 'name_en'])
                ->innerJoin('country', 'tournament.country = country.id')
                ->where('date_from >= ' . date('Y-m-d'))
                ->orderBy('date_from'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('index', [
            'clubs' => $clubs,
            'agendaListDataProvider' => $agendaDataProvider
        ]);
    }
    public function actionLayout()
    {
        $this->layout='layout';
        $clubs = Club::listClubsByCountry()->all();

        $agendaDataProvider = new ActiveDataProvider([
            'query' => Tournament::find()
                ->select( ['DATE_FORMAT(date_from, "%M %Y") AS m_date', 'date_from', 'date_to', 'name', 'slug', 'team_max', 'city', 'country', 'name_en'])
                ->innerJoin('country', 'tournament.country = country.id')
                ->where('date_from >= ' . date('Y-m-d'))
                ->orderBy('date_from'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('index', [
            'clubs' => $clubs,
            'agendaListDataProvider' => $agendaDataProvider
        ]);
    }
    public function actionCommunity()
    {
        return $this->render('community');
    }
    public function actionRules()
    {
        return $this->render('rules');
    }
    public function actionNews()
    {
        return $this->render('news');
    }

    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->session->setFlash('success', 'You were logged out correctly!');

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('success', 'contactFormSubmitted');
            //customAlert::setCustomFlash('success', 'Hurra', 'Message sent!');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAddAdmin() {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = Yii::$app->params['adminEmail'];
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                //customAlert::setCustomFlash('success', 'Hurra', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
                //customAlert::setCustomFlash('error', 'Error =(', 'Sorry, we are unable to reset password for email provided.');
            }

        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            //customAlert::setCustomFlash('success', 'Hurra', 'New password was saved.');
            return $this->redirect('/site/login');
        }

        return $this->render('resetPassword', [
            'model' => $model,
            ]);
      }

}
