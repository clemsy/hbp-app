<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Country;
use app\models\Club;
use yii\web\UploadedFile;

class ClubController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['edit', 'update', 'create'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $models = $this->listClubsByCountry()->all();

        return $this->render('index', [
            'models' => $models,
        ]);
    }
    public function actionMap()
    {

        return $this->render('map', []);
    }

    public function actionJson()
    {

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        echo json_encode([
            "markers" =>  [
                ["lat"=>-37.8210922667, "lng"=>175.2209316333, "popup" => "2"],
                ["lat"=>-37.8210819833, "lng"=>175.2213903167, "popup" => "3"],

            ]
        ]);
    }

    public function actionSlug($slug)
    {
        $model = Club::find()->where(['slug'=>$slug])->one();
        if (!is_null($model)) {
            return $this->render('view', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect('/club/index');
        }
    }
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if(!$model)
        {
            return $this->redirect('index');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {

        if(Club::findByUserId(Yii::$app->getUser()->getId())) {
            Yii::$app->session->setFlash('danger', 'Sorry, you already added your club. ');
            // Yii::$app->session->setFlash('', 'You were logged out correctly!');
            return $this->redirect('index');
        }

        //$model = new \app\models\Club(['scenario' => 'create']);
        $model = new Club();

        if ($model->load(Yii::$app->request->post())) {

            if (!Yii::$app->user->isGuest) {
                $model->user_id = Yii::$app->getUser()->getId();
            }else{
                $model->user_id = User::createTempUser($model->email_contact);
            }


            if ($model->validate()) {
                $file = UploadedFile::getInstance($model, 'image_logo');
                if(!empty($file)) {

                    $file->saveAs('uploads/clubs/' . $file->baseName . '.' . $file->extension);
                    $model->image_logo = $file->baseName . '.' . $file->extension;
                }

                $model->save();

                Club::sendModeratorEmail($model->id);

                // return $this->redirect(['view', 'id'=>$model->id]);
                return $this->render('thankyou', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $old_image_logo = $model->image_logo;

        if(!Yii::$app->user->identity->is_admin AND Yii::$app->user->id!=$model->user_id) {
            Yii::$app->session->setFlash('danger', 'Sorry, you are not allowed to edit that club. ');
            return $this->redirect('index');
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {

                if(isset($_POST['Club']['image_logo'])) {
                    $file = UploadedFile::getInstance($model, 'image_logo');

                    if (!empty($file->baseName)) {
                        $file->saveAs('uploads/clubs/' . $file->baseName . '.' . $file->extension);
                        $model->image_logo = $file->baseName . '.' . $file->extension;
                    }else{
                        $model->image_logo = $old_image_logo;
                    }


                }

               $model->save();

                Yii::$app->session->setFlash('success', 'Club information saved!');
                return $this->redirect(['clubs/'.$model->slug]);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    protected function uploadLogo($file) {

    }
    protected function findModel($id)
    {
        if (($model = Club::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function listClubs()
    {

        $query = (new \yii\db\Query())
            ->select([
                '*'
            ])
            ->from('club')
            ->innerJoin('country', 'club.country = country.id')
            ->innerJoin('user', 'club.user_id = user.id');
            //->where(['is_active' => 1])
            //->orderBy('date_from ASC');


        return $query;
    }
    protected function listClubsByCountry()
    {

        $query = (new \yii\db\Query())
            ->select([
                '*'
            ])
            ->from('club')
            ->innerJoin('country', 'club.country = country.id');

            //->innerJoin('user', 'club.user_id = user.id')
            if(!Yii::$app->user->isGuest AND Yii::$app->user->identity->is_admin) {

            }else{
                $query->where(['is_validated' => 1]);
            }
            $query->orderBy('country.name_en ASC');


        return $query;
    }
}
