<?php

namespace app\controllers;

use Yii;
use app\models\Tournament;
use app\models\Stage;
use yii\web\Controller;
use yii\filters\AccessControl;

class TournamentController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'display'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $models = Tournament::find()
            ->select( ['DATE_FORMAT(date_from, "%M %Y") AS m_date', 'date_from', 'date_to', 'name', 'slug', 'city', 'country'])
            ->where('date_from >= ' . date('Y-m-d'))
            ->orderBy('date_from')
            ->all();
        return $this->render('index', [
            'models' => $models,
        ]);
    }

    public function actionSlug($slug)
    {
        $model = Tournament::find()->where(['slug'=>$slug])->one();
        if (!is_null($model)) {
            return $this->render('display', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect('/club/index');
        }
    }
    public function actionDisplay($id)
    {
        $model = $this->findModel($id);

        if(!$model)
        {
            return $this->redirect('index');
        }

        return $this->render('display', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {
        //$model = new Tournament(['scenario' => 'create']);
        $model = new Tournament();
        $stages = [new Stage];

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->save();
                // echo "saved";
                return $this->redirect(['display', 'id'=>$model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'stages' => (empty($stages)) ? [new Stage] : $stages
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $stages = $model->getStages()->all();


        if($model->creator!=Yii::$app->user->getId()) {
            Yii::$app->session->setFlash('success', 'Sorry, you are not allowed to edit that tournament.');
           // Yii::$app->session->setFlash('', 'You were logged out correctly!');
            $this->redirect('index');
        }

        //$model->scenario = Tournament::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save();
            // customAlert::setCustomFlash('info', 'success', 'your profile has been updated', 5000);
            return $this->redirect(['display', 'id'=>$model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'stages' => (empty($stages)) ? [new Stage] : $stages
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Tournament model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tournament the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tournament::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function listTournaments()
    {

        $query = (new \yii\db\Query())
            ->select([
                '*'
            ])
            ->from('tournament')
            ->innerJoin('country', 'tournament.country = country.id')
            ->innerJoin('user', 'tournament.user_id = user.id')
            ->where(['is_active' => 1])
            ->orderBy('date_from ASC');


        return $query;
    }
}
