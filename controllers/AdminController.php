<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;

class AdminController extends \yii\web\Controller
{


    public function beforeAction($action)
    {
        if(!Yii::$app->user->identity->is_admin) {
            return $this->goHome();
        }
        // $this->layout = 'admin'; //your layout name
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
