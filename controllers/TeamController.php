<?php

namespace app\controllers;

use Yii;
use app\models\Team;
use app\models\Player;
use app\models\TeamPlayer;
use app\models\Country;
use app\models\Club;



class TeamController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new Team(['scenario' => 'create']);
        $players = [new Player()];

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'players' => (empty($players)) ? [new Player] : $players
        ]);
    }

}
