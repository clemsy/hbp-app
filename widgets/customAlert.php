<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\widgets;


use yii;
use yii\base\Widget;
use kartik\widgets\AlertBlock;
use kartik\growl\Growl;
use yii\i18n\Formatter;

/**
 * Description of customAlert
 *
 * @author clement
 */
class customAlert  extends Widget
{
    
    public static $formatter = Yii\i18n\Formatter;
    
    /* Sends  alerts to the view
     * 
     */
    public static function showAlert()
    {
        
        $flashMessages = Yii::$app->session->getAllFlashes();
        if($flashMessages) {
         
            foreach ($flashMessages as $k => $message)
            {
                if ($k=='success') {
                    $type = Growl::TYPE_SUCCESS;
                    $icon = 'ok';
                } elseif ($k=='error') {
                    $type = Growl::TYPE_DANGER;
                    $icon = 'exclamation';
                } else {
                    $type = Growl::TYPE_INFO;
                    $icon = 'info';
                }
                
                // $type = Yii::$app->params['components']['alerts']['type'][$k]['type'];
                // $icon = Yii::$app->params['components']['alerts']['type'][$k]['icon'];
                    
                self::alert($type,$icon,$message[1]);
            }
            
        }
        
        
    }
    /* Sets a Flash with custom options
     * @property string $type : string [info,success,error]
     * @property string $title : alert title, localised with yii::t('app', $title);
     * @property string $body : alert body, formatted asNtext and localised with yii::t('app', $title);
     * @property string $delay : todo
     */
    public static function setCustomFlash($type, $title, $body, $delay = null) {
        
       Yii::$app->session->setFlash($type, ['1' => ['delay' => $delay, 'title' => Yii::t('app', $title), 'body'=>Yii::$app->formatter->asNtext(Yii::t('app', $body))]]);
       return true;
    }

    /* Sends an alert to the view
     * @property string $type : string [info,success,error]
     * @property string $icon : glyphicon associated to the title
     * @property array $message array('title', 'body') of the message alert
     */
        public static function alert($type,$icon,$message=array('title'=>null, 'body'=>null),$delay=null) {

          //  die(print_r($message));

        return Growl::widget([
                'type' => $type,
                'title' => $message['title'],
                'icon' => 'glyphicon glyphicon-'.$icon.'-sign',
                'body' => $message['body'],
                'showSeparator' => true,
                'delay' => false,
                'pluginOptions' => [
                    'delay' => !is_null($delay) ? $delay : Yii::$app->params['alerts']['delay'],
                    'showProgressbar' => false,
                    'placement' => [
                        'from' => 'top',
                        'align' => 'right',
                    ]
                ]
            ]);
    }
   
}
