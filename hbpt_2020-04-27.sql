# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Hôte: localhost (MySQL 5.5.5-10.3.12-MariaDB)
# Base de données: hbpt
# Temps de génération: 2020-04-27 07:04:34 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table club
# ------------------------------------------------------------

DROP TABLE IF EXISTS `club`;

CREATE TABLE `club` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` int(11) unsigned NOT NULL,
  `is_real_club` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `email_contact` int(11) DEFAULT NULL,
  `active_players` int(11) DEFAULT NULL,
  `creation_date` int(4) unsigned DEFAULT NULL,
  `training_court_address` varchar(255) DEFAULT NULL,
  `training_info` text DEFAULT NULL,
  `court_lat` varchar(255) DEFAULT NULL,
  `court_lng` varchar(255) DEFAULT NULL,
  `image_logo` varchar(255) DEFAULT NULL,
  `image_banner` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `history` text DEFAULT NULL,
  `url_facebook` varchar(255) DEFAULT NULL,
  `url_instagram` varchar(255) DEFAULT NULL,
  `url_website` varchar(255) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_validated` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country` (`country`),
  KEY `club_user` (`user_id`),
  CONSTRAINT `club_ibfk_1` FOREIGN KEY (`country`) REFERENCES `country` (`id`),
  CONSTRAINT `club_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `club` WRITE;
/*!40000 ALTER TABLE `club` DISABLE KEYS */;

INSERT INTO `club` (`id`, `name`, `city`, `country`, `is_real_club`, `user_id`, `email_contact`, `active_players`, `creation_date`, `training_court_address`, `training_info`, `court_lat`, `court_lng`, `image_logo`, `image_banner`, `description`, `history`, `url_facebook`, `url_instagram`, `url_website`, `is_active`, `is_validated`)
VALUES
	(1,'Brussels Bike Polo','Brussels',18,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'Zurïch Bike Polo','Zurïch',217,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `club` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `number` smallint(3) NOT NULL,
  `alpha` varchar(2) NOT NULL,
  `calling` smallint(3) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `name_uk` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alpha` (`alpha`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;

INSERT INTO `country` (`id`, `number`, `alpha`, `calling`, `name_en`, `name_ru`, `name_uk`)
VALUES
	(1,4,'af',93,'Afghanistan','Афганистан','Афганістан'),
	(2,8,'al',355,'Albania','Албания','Албанія'),
	(3,10,'aq',672,'Antarctica','Антарктика','Антарктика'),
	(4,12,'dz',213,'Algeria','',''),
	(5,16,'as',1,'American Samoa','',''),
	(6,20,'ad',376,'Andorra','Андора','Андора'),
	(7,24,'ao',244,'Angola','Ангола','Ангола'),
	(8,28,'ag',1,'Antigua and Barbuda','',''),
	(9,31,'az',994,'Azerbaijan','',''),
	(10,32,'ar',54,'Argentina','Аргентина','Аргентина'),
	(11,36,'au',61,'Australia','Австралия','Австралія'),
	(12,40,'at',43,'Austria','Австрия','Австрія'),
	(13,44,'bs',1,'The Bahamas','',''),
	(14,48,'bh',973,'Bahrain','',''),
	(15,50,'bd',880,'Bangladesh','',''),
	(16,51,'am',374,'Armenia','Армения','Арменія'),
	(17,52,'bb',1,'Barbados','',''),
	(18,56,'be',32,'Belgium','Белгия','Белгія'),
	(19,60,'bm',1,'Bermuda','',''),
	(20,64,'bt',975,'Bhutan','',''),
	(21,68,'bo',591,'Bolivia','Боливия','Болівія'),
	(22,70,'ba',387,'Bosnia and Herzegovina','',''),
	(23,72,'bw',267,'Botswana','',''),
	(24,74,'bv',47,'Bouvet Island','',''),
	(25,76,'br',55,'Brazil','Бразилия','Бразилія'),
	(26,84,'bz',501,'Belize','',''),
	(27,86,'io',246,'British Indian Ocean Territory','',''),
	(28,90,'sb',677,'Solomon Islands','',''),
	(29,92,'vg',1,'British Virgin Islands','',''),
	(30,96,'bn',673,'Brunei','',''),
	(31,100,'bg',359,'Bulgaria','',''),
	(32,104,'mm',95,'Myanmar','',''),
	(33,108,'bi',257,'Burundi','',''),
	(34,112,'by',375,'Belarus','Белорусия','Білорусія'),
	(35,116,'kh',855,'Cambodia','',''),
	(36,120,'cm',237,'Cameroon','Камерун','Камерун'),
	(37,124,'ca',1,'Canada','',''),
	(38,132,'cv',238,'Cabo Verde','',''),
	(39,136,'ky',1,'Cayman Islands','',''),
	(40,140,'cf',236,'Central African Republic','',''),
	(41,144,'lk',94,'Sri Lanka','',''),
	(42,148,'td',235,'Chad','',''),
	(43,152,'cl',56,'Chile','',''),
	(44,156,'cn',86,'China','',''),
	(45,158,'tw',886,'Taiwan','',''),
	(46,162,'cx',61,'Christmas Island','',''),
	(47,166,'cc',61,'Cocos Islands','',''),
	(48,170,'co',57,'Colombia','',''),
	(49,174,'km',269,'Comoros','',''),
	(50,175,'yt',262,'Mayotte','',''),
	(51,178,'cg',242,'Republic of the Congo','',''),
	(52,180,'cd',243,'Democratic Republic of the Congo','',''),
	(53,184,'ck',682,'Cook Islands','',''),
	(54,188,'cr',506,'Costa Rica','',''),
	(55,191,'hr',385,'Croatia','',''),
	(56,192,'cu',53,'Cuba','',''),
	(57,196,'cy',357,'Cyprus','',''),
	(58,203,'cz',420,'Czech Republic','',''),
	(59,204,'bj',229,'Benin','',''),
	(60,208,'dk',45,'Denmark','',''),
	(61,212,'dm',1,'Dominica','',''),
	(62,214,'do',1,'Dominican Republic','',''),
	(63,218,'ec',593,'Ecuador','',''),
	(64,222,'sv',503,'El Salvador','',''),
	(65,226,'gq',240,'Equatorial Guinea','',''),
	(66,231,'et',251,'Ethiopia','',''),
	(67,232,'er',291,'Eritrea','',''),
	(68,233,'ee',372,'Estonia','',''),
	(69,234,'fo',298,'Faroe Islands','',''),
	(70,238,'fk',500,'Falkland Islands','',''),
	(71,239,'gs',500,'South Georgia and the South Sandwich Islands','',''),
	(72,242,'fj',679,'Fiji','',''),
	(73,246,'fi',358,'Finland','',''),
	(74,248,'ax',358,'?land Islands','',''),
	(75,250,'fr',33,'France','',''),
	(76,254,'gf',594,'French Guiana','',''),
	(77,258,'pf',689,'French Polynesia','',''),
	(78,260,'tf',33,'French Southern and Antarctic Lands','',''),
	(79,262,'dj',253,'Djibouti','',''),
	(80,266,'ga',241,'Gabon','',''),
	(81,268,'ge',995,'Georgia','',''),
	(82,270,'gm',220,'Gambia','',''),
	(83,275,'ps',970,'Palestine','',''),
	(84,276,'de',49,'Germany','',''),
	(85,288,'gh',233,'Ghana','',''),
	(86,292,'gi',350,'Gibraltar','',''),
	(87,296,'ki',686,'Kiribati','',''),
	(88,300,'gr',30,'Greece','',''),
	(89,304,'gl',299,'Greenland','',''),
	(90,308,'gd',1,'Grenada','',''),
	(91,312,'gp',590,'Guadeloupe','',''),
	(92,316,'gu',1,'Guam','',''),
	(93,320,'gt',502,'Guatemala','',''),
	(94,324,'gn',224,'Guinea','',''),
	(95,328,'gy',592,'Guyana','',''),
	(96,332,'ht',509,'Haiti','',''),
	(97,334,'hm',61,'Heard Island and McDonald Islands','',''),
	(98,336,'va',39,'Vatican City','',''),
	(99,340,'hn',504,'Honduras','',''),
	(100,344,'hk',852,'Hong Kong','',''),
	(101,348,'hu',36,'Hungary','',''),
	(102,352,'is',354,'Iceland','',''),
	(103,356,'in',91,'India','',''),
	(104,360,'id',62,'Indonesia','',''),
	(105,364,'ir',98,'Iran','',''),
	(106,368,'iq',964,'Iraq','',''),
	(107,372,'ie',353,'Ireland','',''),
	(108,376,'il',972,'Israel','',''),
	(109,380,'it',39,'Italy','',''),
	(110,384,'ci',225,'Ivory Coast','',''),
	(111,388,'jm',1,'Jamaica','',''),
	(112,392,'jp',81,'Japan','',''),
	(113,398,'kz',7,'Kazakhstan','',''),
	(114,400,'jo',962,'Jordan','',''),
	(115,404,'ke',254,'Kenya','',''),
	(116,408,'kp',850,'North Korea','',''),
	(117,410,'kr',82,'South Korea','',''),
	(118,414,'kw',965,'Kuwait','',''),
	(119,417,'kg',996,'Kyrgyzstan','',''),
	(120,418,'la',856,'Laos','',''),
	(121,422,'lb',961,'Lebanon','',''),
	(122,426,'ls',266,'Lesotho','',''),
	(123,428,'lv',371,'Latvia','',''),
	(124,430,'lr',231,'Liberia','',''),
	(125,434,'ly',218,'Libya','',''),
	(126,438,'li',423,'Liechtenstein','',''),
	(127,440,'lt',370,'Lithuania','',''),
	(128,442,'lu',352,'Luxembourg','',''),
	(129,446,'mo',853,'Macau','',''),
	(130,450,'mg',261,'Madagascar','',''),
	(131,454,'mw',265,'Malawi','',''),
	(132,458,'my',60,'Malaysia','',''),
	(133,462,'mv',960,'Maldives','',''),
	(134,466,'ml',223,'Mali','',''),
	(135,470,'mt',356,'Malta','',''),
	(136,474,'mq',596,'Martinique','',''),
	(137,478,'mr',222,'Mauritania','',''),
	(138,480,'mu',230,'Mauritius','',''),
	(139,484,'mx',52,'Mexico','',''),
	(140,492,'mc',377,'Monaco','',''),
	(141,496,'mn',976,'Mongolia','',''),
	(142,498,'md',373,'Moldova','',''),
	(143,499,'me',382,'Montenegro','',''),
	(144,500,'ms',1,'Montserrat','',''),
	(145,504,'ma',212,'Morocco','',''),
	(146,508,'mz',258,'Mozambique','',''),
	(147,512,'om',968,'Oman','',''),
	(148,516,'na',264,'Namibia','',''),
	(149,520,'nr',674,'Nauru','',''),
	(150,524,'np',977,'Nepal','',''),
	(151,528,'nl',31,'Netherlands','',''),
	(152,531,'cw',599,'Cura?ao','',''),
	(153,533,'aw',297,'Aruba','',''),
	(154,534,'sx',1,'Sint Maarten','',''),
	(155,535,'bq',599,'Caribbean Netherlands','',''),
	(156,540,'nc',687,'New Caledonia','',''),
	(157,548,'vu',678,'Vanuatu','',''),
	(158,554,'nz',64,'New Zealand','',''),
	(159,558,'ni',505,'Nicaragua','',''),
	(160,562,'ne',227,'Niger','',''),
	(161,566,'ng',234,'Nigeria','',''),
	(162,570,'nu',683,'Niue','',''),
	(163,574,'nf',672,'Norfolk Island','',''),
	(164,578,'no',47,'Norway','',''),
	(165,580,'mp',1,'Northern Mariana Islands','',''),
	(166,581,'um',1,'United States Minor Outlying Islands','',''),
	(167,583,'fm',691,'Federated States of Micronesia','',''),
	(168,584,'mh',692,'Marshall Islands','',''),
	(169,585,'pw',680,'Palau','',''),
	(170,586,'pk',92,'Pakistan','',''),
	(171,591,'pa',507,'Panama','',''),
	(172,598,'pg',675,'Papua New Guinea','',''),
	(173,600,'py',595,'Paraguay','',''),
	(174,604,'pe',51,'Peru','',''),
	(175,608,'ph',63,'Philippines','',''),
	(176,612,'pn',64,'Pitcairn Islands','',''),
	(177,616,'pl',48,'Poland','',''),
	(178,620,'pt',351,'Portugal','',''),
	(179,624,'gw',245,'Guinea-Bissau','',''),
	(180,626,'tl',670,'East Timor','',''),
	(181,630,'pr',1,'Puerto Rico','',''),
	(182,634,'qa',974,'Qatar','',''),
	(183,638,'re',262,'R?union','',''),
	(184,642,'ro',40,'Romania','',''),
	(185,643,'ru',7,'Russia','Россия','Росія'),
	(186,646,'rw',250,'Rwanda','',''),
	(187,652,'bl',590,'Saint Barth?lemy','',''),
	(188,654,'sh',290,'Saint Helena, Ascension and Tristan da Cunha','',''),
	(189,659,'kn',1,'Saint Kitts and Nevis','',''),
	(190,660,'ai',1,'Anguilla','',''),
	(191,662,'lc',1,'Saint Lucia','',''),
	(192,663,'mf',590,'Saint Martin','',''),
	(193,666,'pm',508,'Saint Pierre and Miquelon','',''),
	(194,670,'vc',1,'Saint Vincent and the Grenadines','',''),
	(195,674,'sm',378,'San Marino','',''),
	(196,678,'st',239,'S?o Tom? and Pr?ncipe','',''),
	(197,682,'sa',966,'Saudi Arabia','',''),
	(198,686,'sn',221,'Senegal','',''),
	(199,688,'rs',381,'Serbia','',''),
	(200,690,'sc',248,'Seychelles','',''),
	(201,694,'sl',232,'Sierra Leone','',''),
	(202,702,'sg',65,'Singapore','',''),
	(203,703,'sk',421,'Slovakia','',''),
	(204,704,'vn',84,'Vietnam','',''),
	(205,705,'si',386,'Slovenia','',''),
	(206,706,'so',252,'Somalia','',''),
	(207,710,'za',27,'South Africa','',''),
	(208,716,'zw',263,'Zimbabwe','',''),
	(209,724,'es',34,'Spain','',''),
	(210,728,'ss',211,'South Sudan','',''),
	(211,729,'sd',249,'Sudan','',''),
	(212,732,'eh',212,'Western Sahara','',''),
	(213,740,'sr',597,'Suriname','',''),
	(214,744,'sj',47,'Svalbard and Jan Mayen','',''),
	(215,748,'sz',268,'Swaziland','',''),
	(216,752,'se',46,'Sweden','',''),
	(217,756,'ch',41,'Switzerland','',''),
	(218,760,'sy',963,'Syria','',''),
	(219,762,'tj',992,'Tajikistan','',''),
	(220,764,'th',66,'Thailand','',''),
	(221,768,'tg',228,'Togo','',''),
	(222,772,'tk',690,'Tokelau','',''),
	(223,776,'to',676,'Tonga','',''),
	(224,780,'tt',1,'Trinidad and Tobago','',''),
	(225,784,'ae',971,'United Arab Emirates','',''),
	(226,788,'tn',216,'Tunisia','',''),
	(227,792,'tr',90,'Turkey','',''),
	(228,795,'tm',993,'Turkmenistan','',''),
	(229,796,'tc',1,'Turks and Caicos Islands','',''),
	(230,798,'tv',688,'Tuvalu','',''),
	(231,800,'ug',256,'Uganda','',''),
	(232,804,'ua',380,'Ukraine','Украина','Україна'),
	(233,807,'mk',389,'Macedonia','',''),
	(234,818,'eg',20,'Egypt','',''),
	(235,826,'gb',44,'United Kingdom','',''),
	(236,831,'gg',44,'Guernsey','',''),
	(237,832,'je',44,'Jersey','',''),
	(238,833,'im',44,'Isle of Man','',''),
	(239,834,'tz',255,'Tanzania','',''),
	(240,840,'us',1,'United States','',''),
	(241,850,'vi',1,'United States Virgin Islands','',''),
	(242,854,'bf',226,'Burkina Faso','',''),
	(243,858,'uy',598,'Uruguay','',''),
	(244,860,'uz',998,'Uzbekistan','',''),
	(245,862,'ve',58,'Venezuela','',''),
	(246,876,'wf',681,'Wallis and Futuna','',''),
	(247,882,'ws',685,'Samoa','',''),
	(248,887,'ye',967,'Yemen','',''),
	(249,894,'zm',260,'Zambia','','');

/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Affichage de la table match
# ------------------------------------------------------------

DROP TABLE IF EXISTS `match`;

CREATE TABLE `match` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) unsigned NOT NULL,
  `tournament_level` int(11) unsigned NOT NULL,
  `team_left_id` int(11) unsigned NOT NULL,
  `team_right_id` int(11) unsigned NOT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournament_id` (`tournament_id`),
  KEY `tournament_level` (`tournament_level`),
  KEY `team_left_id` (`team_left_id`),
  KEY `team_right_id` (`team_right_id`),
  CONSTRAINT `match_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`),
  CONSTRAINT `match_ibfk_2` FOREIGN KEY (`team_left_id`) REFERENCES `team` (`id`),
  CONSTRAINT `match_ibfk_3` FOREIGN KEY (`team_right_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Affichage de la table migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;

INSERT INTO `migration` (`version`, `apply_time`)
VALUES
	('m000000_000000_base',1552029492),
	('m010101_100001_init_comment',1552029498),
	('m140506_102106_rbac_init',1552029498),
	('m150105_172247_create_cron_schedule_tbl',1552029498),
	('m150212_182851_init_cms',1552029498),
	('m150227_114524_init',1552029498),
	('m150828_085134_init_user_tables',1552029498),
	('m160629_121330_add_relatedTo_column_to_comment',1552029498),
	('m160804_101551_drop_dateSheduled_and_dateExecuted_columns',1552029498),
	('m161109_092304_rename_comment_table',1552029498),
	('m161109_104201_rename_setting_table',1552029498),
	('m161109_105445_rename_cms_table',1552029498),
	('m161109_111305_rename_cron_schedule_table',1552029498),
	('m161109_112016_rename_user_table',1552029498),
	('m161109_121736_create_session_table',1552029498),
	('m161114_094902_add_url_column_to_comment_table',1552029498),
	('m170323_102933_add_description_column_to_setting_table',1552029498),
	('m170413_125133_rename_date_columns',1552029498),
	('m170416_182141_rename_fields',1552029498),
	('m170416_182830_add_markdown_content_column_to_cms_table',1552029498),
	('m170419_224001_create_attachment_table',1552029498),
	('m170608_141511_rename_columns',1552029498),
	('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1552029498),
	('m180523_151638_rbac_updates_indexes_without_prefix',1552029499),
	('m190309_073623_create_user_table',1552117124);

/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table player
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player`;

CREATE TABLE `player` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `club` int(11) unsigned NOT NULL,
  `city` varchar(255) NOT NULL DEFAULT '',
  `country` int(11) unsigned NOT NULL,
  `nationality` int(11) unsigned NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `club` (`club`),
  KEY `nationality` (`nationality`),
  KEY `country` (`country`),
  CONSTRAINT `player_ibfk_2` FOREIGN KEY (`club`) REFERENCES `club` (`id`),
  CONSTRAINT `player_ibfk_3` FOREIGN KEY (`nationality`) REFERENCES `country` (`id`),
  CONSTRAINT `player_ibfk_4` FOREIGN KEY (`country`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;

INSERT INTO `player` (`id`, `name`, `club`, `city`, `country`, `nationality`, `email`, `created_at`)
VALUES
	(1,'clement',1,'Brussels',18,75,'clemsy@gmail.com','2019-04-25 13:03:25');

/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table stage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stage`;

CREATE TABLE `stage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) unsigned NOT NULL,
  `order` int(11) unsigned NOT NULL,
  `points_win` int(11) unsigned NOT NULL,
  `points_defeat` int(11) unsigned NOT NULL,
  `points_draw` int(11) unsigned NOT NULL,
  `rounds` int(11) unsigned NOT NULL,
  `number_of_groups` int(11) unsigned NOT NULL,
  `teams_per_group` int(11) unsigned NOT NULL,
  `number_of_courts` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tournament_id` (`tournament_id`),
  CONSTRAINT `stage_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `stage` WRITE;
/*!40000 ALTER TABLE `stage` DISABLE KEYS */;

INSERT INTO `stage` (`id`, `tournament_id`, `name`, `type`, `order`, `points_win`, `points_defeat`, `points_draw`, `rounds`, `number_of_groups`, `teams_per_group`, `number_of_courts`)
VALUES
	(1,2,'Day 1',1,1,3,2,0,5,2,8,1),
	(2,2,'Day 2',3,2,3,2,0,5,2,8,1);

/*!40000 ALTER TABLE `stage` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team`;

CREATE TABLE `team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` int(11) unsigned NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country` (`country`),
  CONSTRAINT `team_ibfk_1` FOREIGN KEY (`country`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;

INSERT INTO `team` (`id`, `name`, `city`, `country`, `slug`)
VALUES
	(1,'T\'es PD ou Hipster?','Brussels',18,NULL);

/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table team_player
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_player`;

CREATE TABLE `team_player` (
  `team_id` int(11) unsigned NOT NULL,
  `player_id` int(11) unsigned NOT NULL,
  `match_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  UNIQUE KEY `team_id` (`team_id`,`match_id`,`player_id`),
  KEY `team_id_2` (`team_id`),
  KEY `match_id` (`match_id`),
  KEY `player_id` (`player_id`),
  CONSTRAINT `team_player_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  CONSTRAINT `team_player_ibfk_2` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `team_player` WRITE;
/*!40000 ALTER TABLE `team_player` DISABLE KEYS */;

INSERT INTO `team_player` (`team_id`, `player_id`, `match_id`, `name`)
VALUES
	(1,1,0,NULL);

/*!40000 ALTER TABLE `team_player` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table tournament
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tournament`;

CREATE TABLE `tournament` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `creator` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `club_id` int(11) unsigned NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT '',
  `country` int(11) unsigned NOT NULL,
  `team_max` int(11) DEFAULT NULL,
  `player_max` int(11) DEFAULT NULL,
  `points_win` int(1) unsigned NOT NULL DEFAULT 3,
  `points_draw` int(1) unsigned NOT NULL DEFAULT 1,
  `points_loose` int(1) unsigned NOT NULL DEFAULT 0,
  `is_active` int(1) DEFAULT NULL,
  `is_finished` int(1) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `registration_from` timestamp NULL DEFAULT NULL,
  `registration_to` timestamp NULL DEFAULT NULL,
  `registration_type` int(11) DEFAULT NULL,
  `registration_fee` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`creator`),
  KEY `club_id` (`club_id`),
  KEY `country` (`country`),
  KEY `slug` (`slug`),
  CONSTRAINT `tournament_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `user` (`id`),
  CONSTRAINT `tournament_ibfk_2` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`),
  CONSTRAINT `tournament_ibfk_3` FOREIGN KEY (`country`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `tournament` WRITE;
/*!40000 ALTER TABLE `tournament` DISABLE KEYS */;

INSERT INTO `tournament` (`id`, `creator`, `name`, `description`, `club_id`, `location`, `city`, `country`, `team_max`, `player_max`, `points_win`, `points_draw`, `points_loose`, `is_active`, `is_finished`, `date_from`, `date_to`, `registration_from`, `registration_to`, `registration_type`, `registration_fee`, `slug`)
VALUES
	(1,1,'BP Tournament','Test Tournament',1,NULL,NULL,18,12,3,3,2,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,2,'Le Grand Royal 8','☞ HELLO ALL !! ☜\r\n\r\nCome and enjoy the 7th edition of our INTERNATIONNAL BIKE POLO TOURNAMENT in Brussels, LE GRAND ROYAL 8 !\r\n\r\n16 teams from all over europe will face and fight for the title, in a very tipical friendly atmosphere...\r\n2 full days of 12 minutes games on a built-on-purpose court square Morichar (1060)\r\n\r\n☞ FORMAT will be 12 min games, 5 swiss rounds on saturday and double elimination on sunday. please come and do some reffing. we will have a beautiful surprise scoreboard to play with !\r\n\r\n☞ FEES are going to be 60€ / team, including breakfasts, lunches and a free drink on saturday night. you will all be hosted at player\'s places !\r\n\r\n☞ FRIDAY NIGHT meeting and BBQ at HUSH RUSH. infos about dinner registration and fees to come later...\r\n\r\n☞ WELL, hope to see you all there, fingers crossed for the random draw ☜',1,' Place Louis Morichar, 1060 Saint-Gilles','Bruxelles',18,16,3,3,0,2,NULL,NULL,'2019-08-30','2019-09-01',NULL,NULL,NULL,NULL,NULL),
	(3,2,'EHBPC 2019 Zurich - Main Event','EHBPC 2019 Zurich - Main Tournament\n',2,'Zurïch','Zurïch',217,48,3,3,2,0,NULL,NULL,'2019-07-15','2019-07-16',NULL,NULL,NULL,NULL,NULL),
	(4,2,'EHBPC 2019 Zurich - Wildcard','EHBPC 2019 Zurich - Wildcard Tournament\n',2,'Zurïch','Zurïch',217,48,3,3,2,0,NULL,NULL,'2019-07-17','2019-07-20',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tournament` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table tournament_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tournament_level`;

CREATE TABLE `tournament_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tournament_id` int(11) unsigned NOT NULL,
  `type` int(11) NOT NULL,
  `number _of_rounds` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournament_id` (`tournament_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `tournament_level` WRITE;
/*!40000 ALTER TABLE `tournament_level` DISABLE KEYS */;

INSERT INTO `tournament_level` (`id`, `name`, `tournament_id`, `type`, `number _of_rounds`)
VALUES
	(1,'Day 1',0,0,NULL);

/*!40000 ALTER TABLE `tournament_level` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table tournament_match
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tournament_match`;

CREATE TABLE `tournament_match` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `match_id` int(11) unsigned NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Affichage de la table tournament_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tournament_team`;

CREATE TABLE `tournament_team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) unsigned NOT NULL,
  `team_id` int(11) unsigned NOT NULL,
  `team_captain` int(11) unsigned NOT NULL,
  `team_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournament_id` (`tournament_id`),
  KEY `team_id` (`team_id`),
  KEY `team_captain` (`team_captain`),
  CONSTRAINT `tournament_team_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`),
  CONSTRAINT `tournament_team_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `tournament_team` WRITE;
/*!40000 ALTER TABLE `tournament_team` DISABLE KEYS */;

INSERT INTO `tournament_team` (`id`, `tournament_id`, `team_id`, `team_captain`, `team_name`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,'Team 1',NULL,NULL);

/*!40000 ALTER TABLE `tournament_team` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'admin','COqy6bUUwZ_Bg1kgWrP-ZMZSo7wUl3jR','$2y$13$8OZerd7hYfEbc8FuOAZ7rOsPFcZEJg5MP6UKB/4bJQ4gNpe/Wk4NC',NULL,'clemsy@gmail.com',10,1552117292,1552132121),
	(2,'clemsy@me.com','r7XmirQDnoaYJqSNfkr2VSChtoUIQOZd','$2y$13$Kfjtgxwl.KVldHLgCdi.N.FxN5huXYB72Iur85lVfO.IdfjiCjoQ.',NULL,'clemsy@me.com',10,1552132220,1552132220);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
