<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TeamPlayer]].
 *
 * @see TeamPlayer
 */
class TeamPlayerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TeamPlayer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TeamPlayer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
