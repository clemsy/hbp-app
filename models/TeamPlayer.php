<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team_player".
 *
 * @property int $team_id
 * @property int $player_id
 * @property int $match_id
 * @property string $name
 *
 * @property Team $team
 * @property Player $player
 * @property Match $match
 */
class TeamPlayer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'player_id'], 'required'],
            [['team_id', 'player_id', 'match_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['team_id', 'match_id', 'player_id'], 'unique', 'targetAttribute' => ['team_id', 'match_id', 'player_id']],
            [['team_id', 'player_id', 'match_id'], 'unique', 'targetAttribute' => ['team_id', 'player_id', 'match_id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Player::className(), 'targetAttribute' => ['player_id' => 'id']],
           // [['match_id'], 'exist', 'skipOnError' => true, 'targetClass' => Match::className(), 'targetAttribute' => ['match_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'team_id' => Yii::t('app', 'Team ID'),
            'player_id' => Yii::t('app', 'Player ID'),
            'match_id' => Yii::t('app', 'Match ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Player::className(), ['id' => 'player_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(Match::className(), ['id' => 'match_id']);
    }

    /**
     * {@inheritdoc}
     * @return TeamPlayerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TeamPlayerQuery(get_called_class());
    }
}
