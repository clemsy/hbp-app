<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property int $number
 * @property string $alpha
 * @property int $calling
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_uk
 *
 * @property Club[] $clubs
 * @property Player[] $players
 * @property Player[] $players0
 * @property Team[] $teams
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'alpha', 'calling', 'name_en', 'name_ru', 'name_uk'], 'required'],
            [['number', 'calling'], 'integer'],
            [['alpha'], 'string', 'max' => 2],
            [['name_en', 'name_ru', 'name_uk'], 'string', 'max' => 255],
            [['alpha'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Number'),
            'alpha' => Yii::t('app', 'Alpha'),
            'calling' => Yii::t('app', 'Calling'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_uk' => Yii::t('app', 'Name Uk'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasMany(Club::className(), ['country' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::className(), ['nationality' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers0()
    {
        return $this->hasMany(Player::className(), ['country' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['country' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CountryQuery(get_called_class());
    }
}
