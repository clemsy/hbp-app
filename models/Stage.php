<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stage".
 *
 * @property int $id
 * @property int $tournament_id
 * @property string $name
 * @property int $type
 * @property int $order
 * @property int $points_win
 * @property int $points_defeat
 * @property int $points_draw
 * @property int $rounds
 * @property int $number_of_groups
 * @property int $teams_per_group
 * @property int $number_of_courts
 */
class Stage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tournament_id', 'type', 'rounds', 'number_of_groups', 'teams_per_group', 'number_of_courts'], 'required'],
            [['tournament_id', 'type', 'order', 'points_win', 'points_defeat', 'points_draw', 'rounds', 'number_of_groups', 'teams_per_group', 'number_of_courts'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tournament_id' => Yii::t('app', 'Tournament ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'order' => Yii::t('app', 'Order'),
            'points_win' => Yii::t('app', 'Points Win'),
            'points_defeat' => Yii::t('app', 'Points Defeat'),
            'points_draw' => Yii::t('app', 'Points Draw'),
            'rounds' => Yii::t('app', 'Rounds'),
            'number_of_groups' => Yii::t('app', 'Number Of Groups'),
            'teams_per_group' => Yii::t('app', 'Teams Per Group'),
            'number_of_courts' => Yii::t('app', 'Number Of Courts'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return StageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StageQuery(get_called_class());
    }
}
