<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tournament_team".
 *
 * @property int $id
 * @property int $tournament_id
 * @property int $team_id
 * @property int $team_captain
 * @property string $team_name
 * @property string $created_at
 * @property int $updated_at
 *
 * @property Tournament $tournament
 * @property Team $team
 */
class TournamentTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tournament_id', 'team_id', 'team_captain'], 'required'],
            [['tournament_id', 'team_id', 'team_captain', 'updated_at'], 'integer'],
            [['created_at'], 'safe'],
            [['team_name'], 'string', 'max' => 255],
            [['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tournament::className(), 'targetAttribute' => ['tournament_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tournament_id' => Yii::t('app', 'Tournament ID'),
            'team_id' => Yii::t('app', 'Team ID'),
            'team_captain' => Yii::t('app', 'Team Captain'),
            'team_name' => Yii::t('app', 'Team Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournament()
    {
        return $this->hasOne(Tournament::className(), ['id' => 'tournament_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * {@inheritdoc}
     * @return TournamentTeamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TournamentTeamQuery(get_called_class());
    }
}
