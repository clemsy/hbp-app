<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tournament".
 *
 * @property int $id
 * @property int $creator
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property int $club_id
 * @property string $location
 * @property string $city
 * @property int $country
 * @property int $team_max
 * @property int $player_max
 * @property int $points_win
 * @property int $points_draw
 * @property int $points_loose
 * @property int $is_active
 * @property int $is_finished
 * @property string $date_from
 * @property string $date_to
 * @property string $registration_from
 * @property string $registration_to
 * @property int $registration_type
 * @property string $registration_fee
 * @property string $image_poster
 *
 * @property Match[] $matches
 * @property Stage[] $stages
 * @property User $creator0
 * @property Club $club
 * @property Country $country0
 * @property TournamentTeam[] $tournamentTeams
 */
class Tournament extends \yii\db\ActiveRecord
{
    public $m_date;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament';
    }


    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['creator', 'club_id', 'country', 'team_max', 'player_max', 'points_win', 'points_draw', 'points_loose', 'is_active', 'is_finished', 'registration_type'], 'integer'],
            [['description'], 'string'],
            [['club_id', 'country'], 'required'],
            [['date_from', 'date_to', 'registration_from', 'registration_to'], 'safe'],
            [['name', 'location', 'city', 'registration_fee', 'slug', 'image_poster'], 'string', 'max' => 255],
            [['creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator' => 'id']],
            [['club_id'], 'exist', 'skipOnError' => true, 'targetClass' => Club::className(), 'targetAttribute' => ['club_id' => 'id']],
            [['country'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'creator' => Yii::t('app', 'Creator'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'club_id' => Yii::t('app', 'Club ID'),
            'location' => Yii::t('app', 'Location'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'team_max' => Yii::t('app', 'Team Max'),
            'player_max' => Yii::t('app', 'Player Max'),
            'points_win' => Yii::t('app', 'Points Win'),
            'points_draw' => Yii::t('app', 'Points Draw'),
            'points_loose' => Yii::t('app', 'Points Loose'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_finished' => Yii::t('app', 'Is Finished'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'registration_from' => Yii::t('app', 'Registration From'),
            'registration_to' => Yii::t('app', 'Registration To'),
            'registration_type' => Yii::t('app', 'Registration Type'),
            'registration_fee' => Yii::t('app', 'Registration Fee'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatches()
    {
        return $this->hasMany(Match::className(), ['tournament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStages()
    {
        return $this->hasMany(Stage::className(), ['tournament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClub()
    {
        return $this->hasOne(Club::className(), ['id' => 'club_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournamentTeams()
    {
        return $this->hasMany(TournamentTeam::className(), ['tournament_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return TournamentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TournamentQuery(get_called_class());
    }
}
