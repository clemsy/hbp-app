<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TournamentTeam]].
 *
 * @see TournamentTeam
 */
class TournamentTeamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TournamentTeam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TournamentTeam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
