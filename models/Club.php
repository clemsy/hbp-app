<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "club".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $city
 * @property int $country
 * @property int $is_real_club
 * @property int $user_id
 * @property string $email_contact
 * @property string $name_contact
 * @property string $phone_contact
 * @property int $active_players
 * @property int $creation_date
 * @property string $training_court_address
 * @property string $training_info
 * @property string $court_lat
 * @property string $court_lng
 * @property string $image_logo
 * @property string $image_banner
 * @property string $description
 * @property string $history
 * @property string $url_facebook
 * @property string $url_instagram
 * @property string $url_website
 * @property int $is_active
 * @property int $is_validated
 *
 * @property Country $country0
 * @property User $user
 * @property Player[] $players
 * @property Tournament[] $tournaments
 */
class Club extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'club';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country', 'name', 'city', 'email_contact', 'name_contact'], 'required'],
            [['country', 'is_real_club', 'user_id',  'active_players', 'creation_date', 'is_active', 'is_validated'], 'integer'],
            [['training_info', 'description', 'history'], 'string'],
            [['email_contact'], 'email'],
            [['name', 'city', 'training_court_address', 'court_lat', 'court_lng', 'image_logo', 'image_banner', 'url_facebook', 'url_instagram', 'url_website'], 'string', 'max' => 255],
            [['country'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'is_real_club' => Yii::t('app', 'Is Real Club'),
            'user_id' => Yii::t('app', 'User ID'),
            'email_contact' => Yii::t('app', 'Email Contact'),
            'active_players' => Yii::t('app', 'Active Players'),
            'creation_date' => Yii::t('app', 'Creation Date'),
            'training_court_address' => Yii::t('app', 'Training Court Address'),
            'training_info' => Yii::t('app', 'Training Info'),
            'court_lat' => Yii::t('app', 'Court Lat'),
            'court_lng' => Yii::t('app', 'Court Lng'),
            'image_logo' => Yii::t('app', 'Image Logo'),
            'image_banner' => Yii::t('app', 'Image Banner'),
            'description' => Yii::t('app', 'Description'),
            'history' => Yii::t('app', 'History'),
            'url_facebook' => Yii::t('app', 'Url Facebook'),
            'url_instagram' => Yii::t('app', 'Url Instagram'),
            'url_website' => Yii::t('app', 'Url Website'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_validated' => Yii::t('app', 'Is Validated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::className(), ['club' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournaments()
    {
        return $this->hasMany(Tournament::className(), ['club_id' => 'id'])->orderBy('date_from');
    }

    /**
     * {@inheritdoc}
     * @return ClubQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClubQuery(get_called_class());
    }


    public static function findByUserId($user_id)
    {
        return static::findOne(['user_id' => $user_id]);
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public static function sendModeratorEmail($club_id)
    {
        /* @var $user Club */
        $club = Club::findOne([
            'id' => $club_id,
        ]);

        if (!$club) {
            return false;
        }

        $email =  Yii::$app->mailer
            ->compose(
                ['html' => 'newClub-html', 'text' => 'newClub-text'],
                ['club' => $club]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setSubject('EURO BIKEPOLO - New club added!')
            ->setTo(Yii::$app->params['adminEmail']);

        foreach (Yii::$app->params['moderators'] as $moderator) {
            $email->setCc($moderator);
        }

        return $email->send();
    }
    public static function listClubsByCountry()
    {

        $query = (new \yii\db\Query())
            ->select([
                '*'
            ])
            ->from('club')
            ->innerJoin('country', 'club.country = country.id');

        //->innerJoin('user', 'club.user_id = user.id')
        if(!Yii::$app->user->isGuest AND Yii::$app->user->identity->is_admin) {

        }else{
            $query->where(['is_validated' => 1]);
        }
        $query->orderBy('country.name_en ASC');


        return $query;
    }
}
