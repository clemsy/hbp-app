<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'European Hardcourt Bikepolo Association' ,
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'places' => [
            'class' => '\dosamigos\google\places\Places',
            'key' => 'AIzaSyCCV0LkwZSjyhSBrEmFgRHut345AHdZUmw',
            'format' => 'json', // or 'xml'
            'forceJsonArrayResponse' => true // for decoding responses to arrays instead of objects
        ],
        'placesSearch' => [
            'class' => '\dosamigos\google\places\Search',
            'key' => 'AIzaSyCCV0LkwZSjyhSBrEmFgRHut345AHdZUmw',
            'format' => 'json' // or 'xml'
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'EoJjWEjyQsBM0zWx-OJcbgXmLDA8U7CS',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '51.83.44.51',
                'username' => 'system@hbp.ovh',
                'password' => '9t#8oiX6',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    ],
                ],
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'clubs' => 'club/index',
                'clubs/index' => 'club/index',
                'clubs/create' => 'club/create',
                'clubs/view/<id:\d+>' => 'club/view',
                'clubs/edit/<id:\d+>' => 'club/edit',
                'clubs/delete/<id:\d+>' => 'club/delete',
                'clubs/<slug>' => 'club/slug',
                'calendar/<slug>' => 'tournament/slug',
                'calendar' => 'tournament/index',
                'rules' => 'site/rules',
                'about' => 'site/about',
                'news' => 'site/news',
                'community' => 'site/community',
                'defaultRoute' => '/site/index',
            ],
        ],
        /*
        */
    ],
    'params' => $params,

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
