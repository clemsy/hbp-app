<?php

return [
    'shortName' => 'EHBA',
    'adminEmail' => 'clemsy@gmail.com',
    'moderators' => [
        'clemsy@gmail.com',
        'clemsy@me.com'
    ],
    'supportEmail' => 'system@hbp.ovh',
    'user.passwordResetTokenExpire' => 3600,
    'bsVersion' => '4.2', // this will set globally `bsVersion` to Bootstrap 4.x for all Krajee Extensions
    'alerts' => [
        'delay' => 8000,
        'type' => [
            'info' => [
                'type' => 'Growl::TYPE_INFO',
                'icon' => 'info',
            ],
            'success' => [
                'type' => 'Growl::TYPE_SUCCESS',
                'icon' => 'ok',
            ],
            'error' => [
                'type' => 'Growl::TYPE_WARNING',
                'icon' => 'exclamation',
            ],
        ],
    ],
    'boolean' => [
        '1' => 'Yes',
        '0' => 'No',
        //'9' => 'Not Sure',
    ],
    'stage' => [
        'type' => [
            '1' => 'Round Robin',
            '2' => 'Swiss',
            '3' => 'Single Elimination',
            '4' => 'Double Elimination',
        ]
    ]
];
