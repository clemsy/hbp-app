<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?><!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> - European Hardcourt Bikepolo Association</title>
    <?php $this->head() ?>

    <!-- Bootstrap CSS -->
    <title><?= Html::encode($this->title) ?> - European Hardcourt Bikepolo Association</title>

    <style>
        body, html {
            background-color: #3c3c3c;
        }
        section {
            padding: 40px 0;
        }
        .navbar {
            padding: 0;
        }
        .jumbotron {
            border-radius: 0;
        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>


<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <?php echo $this->render('_navbar'); ?>

</header><!-- End Header -->


<?= Breadcrumbs::widget([
    'options'=> ['class'=>'bg-dark'],
    'itemTemplate' => "\n\t<li class=\"breadcrumb-item\"><i>{link}</i></li>\n", // template for all links
    'activeItemTemplate' => "\t<li class=\"breadcrumb-item active\">{link}</li>\n", // template for the active link
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>


<?php
foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . ' alert-dismissible fade show" role="alert">'
        . $message
        .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button></div>';

}
?>

<main id="main">

    <!-- ======= About Section ======= -->
    <section >
        <div class="container-lg">
            <?=$content;?>
            <div>
    </section>
</main>


<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">&copy; <?=date('Y');?> European Hardcourt BikePolo Association</span>
    </div>
</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>