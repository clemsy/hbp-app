<?php
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\Html;
use yii\helpers\Url;

NavBar::begin([
    'innerContainerOptions' => ['class' => 'container-fluid'],

    'brandLabel' => Yii::$app->params['shortName'],
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-expand-md bg-black text-white navbar-dark justify-content-end',
    ],
]);

$menuItems = [
   // ['label' => 'Home', 'url' => ['/site/index']],
    ['label' => 'Clubs', 'url' => ['/clubs']],
    ['label' => 'Calendar', 'url' => ['/calendar']],
    ['label' => 'News', 'url' => ['/news']],
    ['label' => 'Community', //'url' => ['/community']],
        'items' => [
                ['label' => 'Forum', 'url' => 'https://eurobikepolo.microco.sm/'],
                ['label' => 'Discord', 'url' => 'https://eurobikepolo.microco.sm/'],
        ]
        ],
    //['label' => 'EHBA Ruleset', 'url' => ['/rules']],
    ['label' => 'About Hardcourt Bikepolo',
        'items'=> [
            ['label' => 'History', 'url' => ['/about']],
            ['label' => 'EHBA Ruleset', 'url' => ['/rules']],
            ['label' => 'About', 'url' => ['/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],

        ],
    ],
    // ['label' => 'Contact', 'url' => ['/site/contact']],
];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}



if(!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin)
{

}

echo Nav::widget([
    'options' => [
        'class' => ['navbar navbar-nav  bg-black ml-auto'],
        'id'=>'mainNav'
    ],
    'items' => $menuItems,
]);
NavBar::end();
?>
    <!-- Fixed navbar
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Fixed navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    -->