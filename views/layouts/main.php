<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> - European Hardcourt Bikepolo Association</title>
    <?php $this->head() ?>

</head>
<body class=" h-100 bg-dark">
<?php $this->beginBody() ?>

<header class="bg-dark">
    <?php echo $this->render('_navbar'); ?>
</header>

<!-- Begin page content -->
<main role="main" class="container bg-main  h-100">
    <section >
        <div class="container  pl-0 pr-0" style="">


        <?= Breadcrumbs::widget([
                'options'=> ['class'=>'bg-dark '],
            'itemTemplate' => "\n\t<li class=\"breadcrumb-item\"><i>{link}</i></li>\n", // template for all links
            'activeItemTemplate' => "\t<li class=\"breadcrumb-item active\">{link}</li>\n", // template for the active link
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>


        <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . ' alert-dismissible fade show" role="alert">'
                    . $message
                    .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button></div>';

        }
        ?>


        </div>
    </section>
    <section class="content bg-main text-light" role="">
        <?= $content ?>
    </section>
</main>

<footer class="footer  bg-dark  ">
    <div class="container">

        <?php
        $menuItems[] = ['label' => 'EHBA', 'url' => ['/']];
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Register', 'url' => ['/site/register']];
            $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        } else {
            $menuItems[] =  Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
            ;
        }

        echo Nav::widget([
            'options' => [
                'class' => ['nav justify-content-left'],
                'id'=>'footerNav'
            ],
            'items' => $menuItems,
        ]);

        ?>

    </div>
</footer>

<?php /*
<footer class="footer">


    <div class="container">
        <span class="text-muted">&copy; <?=date('Y');?> European Hardcourt BikePolo Association</span>


            if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Register', 'url' => ['/site/register']];
            $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
            $menuItems[] =  Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                ;
            }

            echo Nav::widget([
            'options' => [
            'class' => [''],
            'id'=>'mainNav'
            ],
            'items' => $menuItems,
            ]);
    </div>
</footer>
        */ ?>

<!--
-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
