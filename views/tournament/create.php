<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tournament */
/* @var $form ActiveForm */
?>
<div class="tournament-create">

    <?= $this->render('forms/tournament', ['model' => $model]);?>
    <hr>
    <?php /*
    echo $this->render('forms/stage', [
            'model' => $model,
            'stages' => $stages
    ]);
    */ ?>

</div><!-- tournament-create -->
