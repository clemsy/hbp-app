<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Country;
use app\models\Club;
use yii\bootstrap4\Modal;
//use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Tournament */
/* @var $form yii\widgets\ActiveForm */



$countries = Country::find()->all();
$countries_name = ArrayHelper::map($countries, 'id', 'name_en');
$countries_code = ArrayHelper::map($countries, 'id', 'alpha');

?>
<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" crossorigin="anonymous"></script>

<?php $form = ActiveForm::begin([
    'id' => 'tournament-edit-form',
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'options' => [
        'scrollToError' => true,
        'validateOnSubmit' => true,
        'class' => 'form'
    ],
]); ?>

<?php if(count($model->errors)>0): ?>
    <div class="has-error" style="background-color: #ffd62f;padding:10px">
        <div class="help-block">
            <?= $form->errorSummary($model); ?>
        </div>
    </div>
<?php endif; ?>


<h3>Tournament Information</h3>


<?= $form->field($model, 'creator')->hiddenInput(['value'=>Yii::$app->user->getId()])->label(false); ?>
<?= $form->field($model, 'is_active')->hiddenInput()->label(false);?>

<hr>
<h3>Main Information</h3>
<?php if(Yii::$app->user->identity->is_admin): ?>
    <?= $form->field($model, 'club_id')->dropDownList( ArrayHelper::map(Club::find()->all(), 'id', 'name'),['prompt'=>'Choose Club'])->label('Organizer') ?>
<?php else: ?>
    <?php $c = Club::find()->where(['user_id'=>Yii::$app->getUser()->getId()])->one();  $c->id; ?>
    <?= $form->field($model, 'club_id')->hiddenInput(['value'=> $c->id])->label(false); ?>
    <label>Organizer club</label>
    <h4><?=$c->name;?></h4>
<?php endif; ?>


<?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required'])->label('Tournament\'s name') ?>
<?= $form->field($model, 'date_from')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter date from ...'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]
]); ?><?= $form->field($model, 'date_to')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter date from ...'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]
]); ?>
<?= $form->field($model, 'description')->textarea(['class'=> 'form-control input-lg required']) ?>


<hr>
<h3>Location Information</h3>
<?= $form->field($model, 'location')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required']) ?>
<?= $form->field($model, 'city')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required']) ?>
<?= $form->field($model, 'country')->dropDownList(
    ArrayHelper::map($countries, 'id', 'name_en'),
    ['prompt'=>'Select country']
) ?>


<hr>
<h3>Game Information</h3>

<?= $form->field($model, 'team_max')->textInput(['type' => 'number', 'class'=> 'form-control input-lg required']) ?>
<p class="small" style="margin-top:-10px">Max Team in the tournament</p>

<?= $form->field($model, 'player_max')->textInput(['type' => 'number', 'class'=> 'form-control input-lg required']) ?>
<p class="small" style="margin-top:-10px">Max players per team</p>

<div class="row">
    <div class="col">

        <?= $form->field($model, 'image_poster')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'overwriteInitial' => true,
                'showUpload' => false,
                'previewFileType' => !empty($model->image_poster) ? 'image' : '',
                'initialPreview'=>
                    !empty($model->image_poster) ? [Html::img("/uploads/calendar/" . $model->image_poster)] : false
                ,
            ],
        ]); ?>

    </div>
</div>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


