<?php
use yii\helpers\Html;
use yii\helpers\Url;
use wbraganca\dynamicform;
use rmrevin\yii\fontawesome\FAS;
/* @var $this yii\web\View */
/* @var $model app\models\Tournament */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Calendar', 'url' => ['/calendar']];
$this->params['breadcrumbs'][] = $this->title ;

?>


<span class="float-right">
    <a href="<?=Url::to(['update', 'id'=>$model->id]);?>"><?= FAS::icon('cog')->size(FAS::SIZE_2X);?></a>
</span>
<h1><?php echo $model->name; ?></h1>
<h3><?php echo $model->getClub()->one()->name; ?></h3>
<h2>
    <?php echo $model->city; ?>
    <?php echo $model->getCountry()->one()->name_en; ?>
    -
    <?php echo $model->date_from; ?>
    <?php echo $model->date_to; ?>
</h2>

<hr>

<div class="well ">
    <?=Yii::$app->formatter->asNtext($model->description);?>
</div>

<br>



