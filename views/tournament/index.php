<?php

use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\ArrayHelper;
$formatter = \Yii::$app->formatter;
$countries = app\models\Country::find()->all();
$countries_name = ArrayHelper::map($countries, 'id', 'name_en');

/* @var $this yii\web\View */
/* @var $model app\models\Tournament */


$this->title = 'Calendar';
$this->params['breadcrumbs'][] = $this->title;
?>
<span class="float-right">
    <a href="/tournament/create"><?= FAS::icon('plus-square')->size(FAS::SIZE_2X);?></a>
</span>

<h1>Calendar</h1>


<ul class="list-unstyled">
    <?php foreach ($models as $model): ?>

    <?php if(!isset($current_month) OR $current_month!=$model->m_date): ?>
        <h3 class="mb-3"><?=$model->m_date;?></h3>
    <?php endif; ?>

    <li class="pl-2 mb-3">
        <a href="<?=Url::to(['/calendar/'.$model->slug]);?>">
            <h4 class="mb-0">
                <?php echo $model->name; ?>
            </h4>
        </a>
        <?=$model->city;?>, <?=$countries_name[$model->country];?>
        <!-- date format : http://userguide.icu-project.org/formatparse/datetime -->
        <br>
        from <?=$formatter->asDate($model->date_from, 'MMMM d');?>
        to <?=$formatter->asDate($model->date_to, 'MMMM d');?>
        <br>

    </li>
        <?php $current_month = $model->m_date; ?>

    <?php endforeach; ?>
</ul>