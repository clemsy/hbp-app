<?php

use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $model app\models\Tournament */
?>

<div class="row">
    <div class="col"></div>
    <div class="col-10">

        <ul>
            <?php foreach ($models as $model): ?>

                <li>
                    <a href="<?=Url::to(['//calendar/'.$model->slug]);?>">
                        <?php echo $model->name; ?>
                    </a>
                </li>

            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col"></div>
</div>