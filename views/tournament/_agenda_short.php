<?php


use yii\widgets\ListView;
?>

<table class="table table-sm table-striped text-dark table-hover">
    <thead>
    <tr>
        <th scope="col">Date</th>
        <th scope="col">Event</th>
        <th scope="col">Location</th>
        <th scope="col">Format</th>
    </tr>
    </thead>
    <tbody>
    <?= ListView::widget([
        'options' => [
            'tag' => 'div',
        ],
        'dataProvider' => $agendaListDataProvider,
        'itemView' => function ($model, $key, $index, $widget) {
            // $itemContent = $this->render('//tournament/_list_item',['model' => $model]);


            return $this->render('//tournament/_list_item',['model' => $model]);
        },
        'itemOptions' => [
            'tag' => false,
        ],
        'summary' => '',

        /* do not display {summary} */
        'layout' => '{items}',

        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
            'maxButtonCount' => 4,
            'options' => [
                'class' => 'pagination col-xs-12'
            ]
        ],

    ]);
    ?>
    </tbody>
</table>