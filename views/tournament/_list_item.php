<?php
// YOUR_APP/views/list/_list_item.php

use yii\helpers\Html;

/* @var $model app\models\Tournament */
?>

<tr onclick="location.href='/calendar/<?=$model->slug;?>'" style="cursor: pointer">
    <th scope="row"><h6 class="display-6"><?=Yii::$app->formatter->asDate($model->date_from, 'MMMM d');?> to <?=Yii::$app->formatter->asDate($model->date_to, 'MMM d yy');?></h6></th>
    <td><h6 class="display-6"><a href="/calendar/<?=$model->slug;?>" class="text-dark"><?= Html::encode($model['name']); ?></a></h6></td>
    <td><h6 class="display-6"><?= Html::encode($model['city']); ?>, <?= $model->getCountry()->one()->name_en; ?></h6></td>
    <td><h6 class="display-6"><?= Html::encode($model->team_max); ?> teams</h6></td>
</tr>


<!--
<article class="list-item pl-0 pr-0" data-key="<?= $model['id'] ?>">
    <div
    <h5 class="display-6"><?= Html::encode($model['name']); ?></h5>

</article>
-->
