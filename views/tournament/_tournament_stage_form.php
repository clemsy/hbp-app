<?php

use antkaz\vue\VueAsset;
VueAsset::register($this); // register VueAsset
$this->registerJsFile('https://unpkg.com/axios/dist/axios.min.js');
$this->registerJsFile('https://unpkg.com/vue-select@latest');

?>


<div id="app" class="vue">

    <button v-on:click="addFormElement('text')">Add Stage</button><br>

    <form-input v-for="(field, index) in fields" :field="field"></form-input>
</div>


<template id="form-input">
    <div>
        <label>Stage {{index}}</label>
        <input :type="field.type" :name="'field-'+index" />
        <a href="#" v-on:click.prevent="removeFormElement(index)">remove</a>
    </div>
</template>


<script>
    Vue.component('form-input', {
        template: '#form-input',
        props: ['field', 'i', 'count'],
        methods: {
            removeFormElement: function (index) {
                this.$delete(this.fields, index);
            },

        }
    })

    var app = new Vue({
        el: '#app',
        data: {
            fields: [],
            inputType: '',
            count: 0,
            index: 0,
            message: 'Hello Vue.js!',
            stage: []
        },
        methods: {
            addFormElement: function (val) {
                console.log(++this.index)
                this.fields.push({type: val, placeholder: 'Textbox ' + (++this.count)});
            },
            reverseMessage: function () {
                this.message = this.message.split('').reverse().join('')
            }
        }
    })
</script>

