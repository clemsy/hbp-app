<?php

use yii\helpers\Html;
use yii\helpers\BaseHtml;
//use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Country;

use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;

$countries = Country::find()->all();
$countries_name = ArrayHelper::map($countries, 'id', 'name_en');
$countries_code = ArrayHelper::map($countries, 'id', 'alpha');

$years = array();
for ( $i = date('Y');$i >= 2000; $i--) {
    $years[$i] = $i;
}
/* @var $this yii\web\View */
/* @var $model app\models\Club */
/* @var $form ActiveForm */
?>



<div class="club-edit">

    <h1>Create club</h1>



    <?php if(count($model->errors)>0): ?>
        <div class="has-error" style="background-color: #ffd62f;padding:10px">
            <div class="help-block">
                <?= $form->errorSummary($model); ?>
            </div>
        </div>
    <?php endif; ?>


    <?php     $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?php if(!Yii::$app->user->isGuest AND Yii::$app->user->identity->is_admin): ?>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <?= $form->field($model, 'is_active')->checkbox(array('label'=>''))
                ->label('Is active ?'); ?>
        </div>
        <div class="col-lg-6 col-md-12">
            <?= $form->field($model, 'is_validated')->checkbox(array('label'=>''))
                ->label('Is Validated ?'); ?>        </div>
    </div>

    <?php endif; ?>


    <?php /* $form->field($model, 'user_id')->hiddenInput(['value'=>Yii::$app->user->getId()])->label(false); */ ?>

    <h3>Club main info</h3>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required'])->label('Club Name') ?>
        </div>
        <div class="col-lg-6 col-md-12">

            <?= $form->field($model, 'is_real_club')->dropDownList(
                Yii::$app->params['boolean'],
                ['prompt'=>'Please choose an option '])->label('Is your club registered?') ?>



        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required'])->label('City') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'country')->dropDownList(
                ArrayHelper::map($countries, 'id', 'name_en'),
                ['prompt'=>'Select country']
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'email_contact')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required'])->label('Contact email') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'name_contact')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required'])->label('Contact name') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'phone_contact')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg'])->label('Contact phone') ?>
        </div>
    </div>

    <div class="row">
        <div class="col">

            <?= $form->field($model, 'creation_date')->dropDownList(
                $years,
                ['prompt'=>'Year of creation']
            ) ?>

            <?php /* $form->field($model, 'creation_date')->textInput(['maxlength' => 4, 'class'=> 'form-control input-lg required', 'length' => 4])->label('When was your club created ?') */ ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'active_players')->textInput(['maxlength' => 4, 'class'=> 'form-control input-lg required', 'length' => 4])->label('How many active players does your club count ?') ?>
        </div>
    </div>


    <h3>Training info</h3>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'training_court_address')->textInput(['maxlength' => 255])->label('Training Court Address')->hint('Please enter a geolocalized address. (enter google maps style address)') ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'court_lat')->textInput(['maxlength' => 255])->label('Training Court Latitude')->hint('You can use <a href="https://www.latlong.net/convert-address-to-lat-long.html" target="_blank">this tool</a> to convert your address to lat/long coordinates.') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'court_lng')->textInput(['maxlength' => 255])->label('Training Court Longitude') ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'training_info')->textarea(['class'=> 'form-control input-lg required'])->label('Training Schedule') ?>
        </div>
    </div>


    <h3>Club Information</h3>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'history')->textarea(['class'=> 'form-control input-lg required']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'description')->textarea(['class'=> 'form-control input-lg required']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'url_website')->textInput(['class'=> 'form-control input required'])->hint('Start with http://...') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'url_facebook')->textInput(['class'=> 'form-control input required'])->hint('https://facebook.com/...') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'url_instagram')->textInput(['class'=> 'form-control input required'])->hint('https://instagram.com/...') ?>
        </div>
    </div>


    <div class="row">
        <div class="col">

            <?= $form->field($model, 'image_logo')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'overwriteInitial' => true,
                'showUpload' => false,
                'previewFileType' => !empty($model->image_logo) ? 'image' : '',
                'initialPreview'=>
                    !empty($model->image_logo) ? [Html::img("/uploads/clubs/" . $model->image_logo)] : false
                ,
            ],
            ]); ?>

        </div>
    </div>

    <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- club-edit -->

