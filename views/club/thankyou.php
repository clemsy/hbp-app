<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'New Club Entry';
$this->params['breadcrumbs'][] = 'Clubs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            Thank you for your club application.
            <br>
            A moderator will validate you entry and make public your club's page.
            <br>
            You will received an email with a login link to complete and edit your club's information and future events.
        </p>
    </div>
</div>
