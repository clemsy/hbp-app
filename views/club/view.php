<?php
use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\ArrayHelper;
use app\models\Country;
//use himiklab\thumbnail\EasyThumbnailImage;


use dosamigos\leaflet\types\LatLng;
use dosamigos\leaflet\layers\Marker;
use dosamigos\leaflet\layers\TileLayer;
use dosamigos\leaflet\LeafLet;
use dosamigos\leaflet\widgets\Map;

/* @var $this yii\web\View */
/* @var $model app\models\Club */

$countries = Country::find()->all();
$countries_name = ArrayHelper::map($countries, 'id', 'name_en');
$countries_code = ArrayHelper::map($countries, 'id', 'alpha');





// first lets setup the center of our map
$center = new LatLng(['lat' => $model->court_lat, 'lng' => $model->court_lng]);
//$center2 = new LatLng(['lat' => '48.860830', 'lng' => '2.409620']);


// now lets create a marker that we are going to place on our map
$marker = new Marker(['latLng' => $center, 'popupContent' => $model->name]);
//$marker2 = new Marker(['latLng' => $center2, 'popupContent' => $model->name]);


// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
// The Tile Layer (very important)
$tileLayer = new TileLayer([
    //'urlTemplate' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    //'urlTemplate' => 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
    'urlTemplate' => 'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=027f5d68627149e891c7c1c4447d69b4',
    'clientOptions' => [
        'attribution' => '<img src="http://developer.mapquest.com/content/osm/mq_logo.png">, ' .
            'Map data &copy; <a href="https://www.opencyclemap.org/">OpenCycleMap.org</a> - The <a href="http://openstreetmap.org">OpenStreetMap</a> Cycle Map'
    ]
]);

// now our component and we are going to configure it
$leaflet = new LeafLet([
    'center' => $center, // set the center
    'zoom' => 7
]);
// Different layers can be added to our map using the `addLayer` function.
$leaflet->addLayer($marker)
    //->addLayer($marker2)      // add the marker
->addLayer($tileLayer);  // add the tile layer



$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Clubs', 'url' => ['/clubs']];
$this->params['breadcrumbs'][] = $countries_name[$model->country];
$this->params['breadcrumbs'][] = $this->title;

?>



<h1><?php echo $model->name; ?></h1>

<div class="clearfix">
    <?php if(!Yii::$app->getUser()->isGuest AND (Yii::$app->getUser()->getId()==$model->user_id OR Yii::$app->user->identity->is_admin)): ?>
        <div class="float-right">
            <a href="<?=Url::to(['edit', 'id'=>$model->id]);?>"><?= FAS::icon('cog');?> Edit Info</a>
        </div>
    <?php endif; ?>

    <h2><?=$model->city; ?>, <?=$countries_name[$model->country]; ?></h2>


    <div class="float-right">

    <?php if(!empty($model->image_logo)): ?>
        <?php /* = EasyThumbnailImage::thumbnailImg('@webroot/uploads/clubs/'.$model->image_logo, 500, 400, EasyThumbnailImage::THUMBNAIL_OUTBOUND, ['class'=>'d-block img-fluid img-thumbnail'], 100);  */ ?>

        <div style=";" class="img-thumbnail">
           <img src="/uploads/clubs/<?=$model->image_logo;?>" style="width: 400px;height:400px">
        </div>
    <?php else: ?>
        <div style="font-size: 100%;" class="img-thumbnail">
            no logo uploaded
        </div>
    <?php endif; ?>
    </div>


<div class="card-body">
    <div class="card-text">
        <dl class="row">
            <dt class="col-3"> Established in</dt>
            <dd class="col-9"><?=$model->creation_date;?></dd>

            <dt class="col-3"> Active players</dt>
            <dd class="col-9"><?=$model->active_players;?></dd>

            <dt class="col-3"> Court Address</dt>
            <dd class="col-9"><?=$model->training_court_address;?> (<a href="https://www.google.com/maps/place/<?=$model->court_lat;?>,<?=$model->court_lng;?>"><?=$model->court_lat;?>,<?=$model->court_lng;?>) </a></dd>

            <dt class="col-3"> Contact Person</dt>
            <dd class="col-9"><?=$model->name_contact;?></dd>

            <dt class="col-3"> Description</dt>
            <dd class="col-9"><?=$model->description;?></dd>

            <dt class="col-3"> History</dt>
            <dd class="col-9"><?=$model->history;?></dd>

            <dt class="col-3"> Trainings</dt>
            <dd class="col-9"><?=$model->training_info;?></dd>

            <dt class="col-3"> Websites</dt>
            <dd class="col-9">
                <ul class="list-unstyled">
                    <?php if(!is_null($model->url_website)): ?>
                    <li>
                        <a href="<?=$model->url_website;?>"><?=$model->url_website;?></a>
                    </li>
                    <?php endif; ?>
                    <?php if(!is_null($model->url_website)): ?>
                    <li>
                        <a href="<?=$model->url_facebook;?>">FaceBook</a>
                    </li>
                    <?php endif; ?>
                    <?php if(!is_null($model->url_website)): ?>
                    <li>
                        <a href="<?=$model->url_instagram;?>">Instagram</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </dd>



        </dl>
    </div>
</div>

</div>
<hr>
<div class=row">
    <div class="col">
        <h3>Club Calendar</h3>
        <?php
            echo $this->render('//tournament/_agenda',  ['models'=> $model->tournaments])
        ;?>
    </div>
</div>
<hr>

<div class=row">
    <div class="col">
        <h3>Club Training court map</h3>
        <?php echo $leaflet->widget(['height'=>'400px']); ?>

        <?php if($model->court_lat && $model->court_lng):?>
            <p class="small text-right">
                <a href="https://www.opencyclemap.org/?zoom=13&lat=<?=$model->court_lat;?>&lon=<?=$model->court_lng;?>=B0000" target="_blank">Open Cycle Map &raquo;</a>
            </p>
        <?php endif; ?>


    </div>
</div>




