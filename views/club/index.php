<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Clubs';
$this->params['breadcrumbs'][] = $this->title;

?>
<h1>Clubs of Eurobikepolo</h1>
<div class="float-right">

    <a href="<?=Url::to('/clubs/create');?>">Add your club</a>
</div>

<ul>
    <?php foreach ($models as $model): ?>

    <?php if(!isset($current_country) OR $current_country!=$model['name_en']): ?>
        <h3><?=$model['name_en'];?></h3>
    <?php endif; ?>
        <li>
            <?= Html::a('<span class="glyphicon glyphicon-eye-open">'.$model['name'].'</span>', '/clubs/'.$model['slug'], ['title' => Yii::t('yii', 'View'),]);  ?>
        </li>
    <?php $current_country = $model['name_en']; ?>
    <?php endforeach; ?>
</ul>


<h2>Map of Europolo Clubs</h2>


<?= $this->render( '_map', ['models'=> $models] ); ?>
