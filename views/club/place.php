
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.3/leaflet.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol/dist/L.Control.Locate.min.css" />

<script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol/dist/L.Control.Locate.min.js" charset="utf-8"></script>

<link rel="stylesheet" href="https://maps.locationiq.com/v2/libs/leaflet-geocoder/1.9.6/leaflet-geocoder-locationiq.min.css">
<script src="https://maps.locationiq.com/v2/libs/leaflet-geocoder/1.9.6/leaflet-geocoder-locationiq.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet-plugins/3.0.2/control/Permalink.js"></script>

<style type="text/css">
    body {
        margin: 0;
    }
    #map {
        width: 0;
        height: 0;
        background-color:transparent;
        display: none;
    }
    #search-box {
        padding-left: 20px;
        padding-top: 20px;
        width: 800px;
        height: 100px;
    }
    #result {
        padding-left: 20px;
        padding-top: 20px;
        width: 800px;
        height: 100px;
    }
    .leaflet-locationiq-results{
        padding-top: 45px;
    }
</style>


<!-- For the invisible map -->
<div id="map"></div>
<!-- For the search box -->
<div id="search-box"></div>
<!-- To display the result -->
<div id="result"></div>


<script>
    // Initialize an empty map without layers (invisible map)
    var map = L.map('map', {
        center: [40.7259, -73.9805], // Map loads with this location as center
        zoom: 12,
        scrollWheelZoom: true,
        zoomControl: false,
        attributionControl: false,
    });

    //Geocoder options
    var geocoderControlOptions = {
        bounds: false,          //To not send viewbox
        markers: false,         //To not add markers when we geocoder
        attribution: null,      //No need of attribution since we are not using maps
        expanded: true,         //The geocoder search box will be initialized in expanded mode
        panToPoint: false       //Since no maps, no need to pan the map to the geocoded-selected location
    }

    //Initialize the geocoder
    var geocoderControl = new L.control.geocoder('f5b8ca122c4d3a', geocoderControlOptions).addTo(map).on('select', function (e) {
        displayLatLon(e.feature.feature.display_name, e.latlng.lat, e.latlng.lng);
    });
</script>
