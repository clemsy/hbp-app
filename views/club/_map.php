<?php
use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\ArrayHelper;
use app\models\Country;
//use himiklab\thumbnail\EasyThumbnailImage;


use dosamigos\leaflet\types\LatLng;
use dosamigos\leaflet\layers\Marker;
use dosamigos\leaflet\layers\TileLayer;
use dosamigos\leaflet\LeafLet;



/* @var $club app\models\Club */

// first lets setup the center of our map
$center = new LatLng(['lat' => '52.3897022', 'lng' => '16.4244081']);

// now lets create a marker that we are going to place on our map
foreach ($models as $club) {
  //  die( print_r ($club));
    if(empty($club['court_lat'])) { continue; }
    $point =  new LatLng(['lat' => $club['court_lat'], 'lng' =>  $club['court_lng']]);
    $markers[] = new Marker(['latLng' => $point, 'popupContent' => $club['name'] . '<br> url'] );
}


// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
// The Tile Layer (very important)
$tileLayer = new TileLayer([
    'urlTemplate' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    //'urlTemplate' => 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
    //'urlTemplate' => 'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=027f5d68627149e891c7c1c4447d69b4',
    'clientOptions' => [
        'attribution' => '<img src="http://developer.mapquest.com/content/osm/mq_logo.png">, ' .
            'Map data &copy; <a href="https://www.opencyclemap.org/">OpenCycleMap.org</a> - The <a href="http://openstreetmap.org">OpenStreetMap</a> Cycle Map'
    ]
]);

// now our component and we are going to configure it
$leaflet = new LeafLet([
    'center' => $center, // set the center
    'zoom' => 4
]);
// Different layers can be added to our map using the `addLayer` function.

foreach ($markers as $marker) {
    $leaflet->addLayer($marker);
}

$leaflet->addLayer($tileLayer);
?>

<?php echo $leaflet->widget(['height'=>'400px']); ?>

