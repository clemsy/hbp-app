<?php


use dosamigos\leaflet\types\LatLng;
use dosamigos\leaflet\layers\Marker;
use dosamigos\leaflet\layers\TileLayer;
use dosamigos\leaflet\LeafLet;
use dosamigos\leaflet\plugins\markercluster\MarkerCluster;
// LeafLet initialization component
$center = new LatLng(['lat' => '53.144890', 'lng' => '29.235330']);

// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
// The Tile Layer (very important)
$tileLayer = new TileLayer([
    'urlTemplate' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    //'urlTemplate' => 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
    //'urlTemplate' => 'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=027f5d68627149e891c7c1c4447d69b4',
    'clientOptions' => [
        'attribution' => '<img src="http://developer.mapquest.com/content/osm/mq_logo.png">, ' .
            'Map data &copy; <a href="https://www.opencyclemap.org/">OpenCycleMap.org</a> - The <a href="http://openstreetmap.org">OpenStreetMap</a> Cycle Map'
    ]
]);

// now our component and we are going to configure it
$leaflet = new LeafLet([
    'center' => $center, // set the center
    'zoom' => 4
]);
// create cluster plugin
$cluster = new MarkerCluster([
'url' =>  Yii::$app->urlManager->createUrl('rest/clubs')

]);

$marker1 = new dosamigos\leaflet\layers\Marker([
    'latLng' => $center,
    'popupContent' => 'Hey! I am a marker'
]);

$marker2 = new dosamigos\leaflet\layers\Marker([
     'latLng' => $center,
     'popupContent' => 'Hey! I am a second marker'
]);


$leaflet->addLayer($tileLayer);
// add them to the cluster plugin
$cluster
    ->addMarker($marker1)
    ->addMarker($marker2);

// install to LeafLet component
$leaflet->plugins->install($cluster);



?>

<?php echo $leaflet->widget(['height'=>'600px']); ?>
