<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>

<div class="row justify-content-center h-100" style="background: no-repeat center/100% url('https://eurobikepolo.com/assets/carousel/History_Banner.jpg'); background-size: cover">
    <div class="col-6 pl-0 pr-0 pt-5">
        <div style="min-height:400px" class=" my-auto align-middle align-self-center text-center">
            <h1 >Welcome to the <strong>European Hardcourt Bikepolo Association</strong> official website</h1>
            <p class="pt-2 ">
                We provide useful content for bike polo lovers.
            </p>

            <div class="text-center">
                <button type="button" class="btn btn-primary btn-lg text-lg-center pl-5 pr-5 "><a href="/clubs" class="text-light">Discover</a></button>
            </div>
        </div>
    </div>
</div>

<div class="row h-100">
    <div class="col-lg-6 col-sm-12 pl-0 pr-0">
        <div class="jumbotron  bg-grad h-100">
        <h2>Clubs of Euro Bikepolo</h2>
            <p class="text-left">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor, risus vitae accumsan mollis, felis mi dignissim velit, nec vulputate arcu ante eget tortor. Vivamus mi metus, efficitur eget diam et, accumsan tincidunt est. Mauris ac dolor urna. Integer vel venenatis metus. In quis justo et tellus condimentum imperdiet et facilisis mauris. Mauris mattis lacinia faucibus. Phasellus consectetur felis nec interdum efficitur.
            </p>
            <div class="text-center">
                <button type="button" class="btn btn-dark btn-sm text-sm-center"><a href="/clubs" class="text-light">Add your Club</a></button>
            </div>

        </div>
    </div>
    <div class="col-lg-6 col-sm-12 pr-0 pl-0 ">
        <div class="bg-light text-right h-100">
            <?= $this->render( '//club/_map', ['models'=> $clubs] ); ?>

            <span class="small pr-4"> <a class="text-dark" href="<?=Url::to('clubs');?>">View all clubs &raquo;</a></span>
        </div>
    </div>
</div>


    <div class="row h-100" style="background: no-repeat center/100% url('/img/banners/Tournaments1.png'); background-size: cover">
        <div class="col p-4 ">

            <h3 class="text-dark" >European Tournament Calendar</h3>
            <p class="text-dark">
                This calendar is regularly updated and show all the Hardcourt Bikepolo events happening in Europe.
                <br>
                If an event is missing or not correctly shown, please contact us or ask your club's representative to add or edit it within your club's info page.
            </p>
            <div class="bg-white">
            <?= $this->render( '//tournament/_agenda_short', ['agendaListDataProvider'=> $agendaListDataProvider] ); ?>
            </div>
            <p class="text-right text-sm-right">
                <a href="<?=Url::to('calendar');?>" class="small">view all tournaments &raquo;</a>
            </p>


        </div>
    </div><div class="container">

</div>

<div class="row h-100 bg-light text-dark">
    <div class="col-12 p-4">

        <h3 class="text-dark" >Community and Discussion</h3>
    </div>

    <div class="col-lg-4 col-sm-12 pt-0 p-2 h-100">

        <div class="text-center">
            <img src="/img/forums.png?a=a" class="rounded" alt="..." style="width: 100%">
        </div>

    </div>

    <div class="col-lg-8 col-sm-12 pt-0 pl-4">
        <h4 class="text-primary">Dedicated forums</h4>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor, risus vitae accumsan mollis, felis mi dignissim velit, nec vulputate arcu ante eget tortor. Vivamus mi metus, efficitur eget diam et, accumsan tincidunt est. Mauris ac dolor urna. Integer vel venenatis metus. In quis justo et tellus condimentum imperdiet et facilisis mauris. Mauris mattis lacinia faucibus. Phasellus consectetur felis nec interdum efficitur.
        </p>

        <div class="text-right">
            <button type="button" class="btn btn-dark text-center"><a href="https://eurobikepolo.microco.sm/" class="text-light">Go to Forums &raquo;</a></button>
        </div>
    </div>

    <div class="border-top my-3"></div>


    <div class="col-lg-8 col-sm-12 pt-0 pl-4">
        <h4 class="text-primary">Discord App</h4>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor, risus vitae accumsan mollis, felis mi dignissim velit, nec vulputate arcu ante eget tortor. Vivamus mi metus, efficitur eget diam et, accumsan tincidunt est. Mauris ac dolor urna. Integer vel venenatis metus. In quis justo et tellus condimentum imperdiet et facilisis mauris. Mauris mattis lacinia faucibus. Phasellus consectetur felis nec interdum efficitur.
        </p>

        <div class="text-right">
            <button type="button" class="btn btn-dark text-center"><a href="<?=Url::to('/community');?>" class="text-light">Go to Discord App &raquo;</a></button>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12 pt-0 p-2 h-100">

        <div class="text-center">
            <img src="/img/discord.jpeg" class="rounded" alt="..." style="width: 100%">
        </div>

    </div>
</div>
