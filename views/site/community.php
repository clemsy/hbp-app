<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Community';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor, risus vitae accumsan mollis, felis mi dignissim velit, nec vulputate arcu ante eget tortor. Vivamus mi metus, efficitur eget diam et, accumsan tincidunt est. Mauris ac dolor urna. Integer vel venenatis metus. In quis justo et tellus condimentum imperdiet et facilisis mauris. Mauris mattis lacinia faucibus. Phasellus consectetur felis nec interdum efficitur.

    </p>


</div>
