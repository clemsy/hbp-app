<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-news">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the News page. You may modify the following file to customize its content:
    </p>

</div>
