<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Country;
use app\models\Club;
use app\models\TeamPlayer;

$countries = Country::find()->all();
$countries_name = ArrayHelper::map($countries, 'id', 'name_en');
$countries_code = ArrayHelper::map($countries, 'id', 'alpha');

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form ActiveForm */
?>
<div class="team-create">

    You are about to register your team for : <strong>EHBPC 2019 - Zurich -  Main Tournament</strong>


    <?php $form = ActiveForm::begin(); ?>


    <h3>Team info</h3>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=> 'form-control input-lg required'])->label('Team Name') ?>

        <?= $form->field($model, 'country')->dropDownList(
            ArrayHelper::map($countries, 'id', 'name_en'),
            ['prompt'=>'Select country']
        ) ?>



    <hr>

    <h3>Players info</h3>



        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>



    <?= $this->render('forms/player', [
        'model' => $model,
        'players' => $players
    ]);?>

</div><!-- team-create -->
