<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $model app\models\Tournament */
/* @var $stage app\models\Stage */
/* @var $form yii\widgets\ActiveForm */

/*
 *
 *  Example :
 *
 *  https://github.com/2amigos/yii2-dynamic-form
 *
 *
 * */



?>
<?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

<div class="panel panel-default">
    <div class="panel-heading"><h3><i class="glyphicon glyphicon-envelope"></i> Team Members</h3></div>
    <div class="panel-body">
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 3, // the maximum times, an element can be cloned (default 999)
            'min' => 3, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $players[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'name',
                'email',
                'city',
                'country',
                'nationality',
                'club',
            ],
        ]); ?>

        <div class="container-items"><!-- widgetContainer -->

            <?php foreach ($players as $i => $player): ?>
            <div class="item panel panel-default"><!-- widgetBody -->
                <div class="panel-heading">
                    <h5 class="panel-title float-left">Stage</h5>
                    <div class="float-right">
                        <button type="button" class="add-item btn btn-success btn-xs"><?= FAS::icon('plus-square');?></button>
                        <button type="button" class="remove-item btn btn-danger btn-xs"><?= FAS::icon('minus-square');?></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php
                    // necessary for update action.
                    if (! $player->isNewRecord) {
                        echo Html::activeHiddenInput($player, '['.$i.']id');
                    }
                    ?>

                    <div class="row">
                        <div class="col">
                            <?= $form->field($player, '['.$i.']name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <?= $form->field($player, '['.$i.']email')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>


                    <?php endforeach; ?>
                </div>


                <div class="form-group">
                    <?= Html::submitButton($player->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php DynamicFormWidget::end(); ?>

                <?php ActiveForm::end(); ?>



                <?php
                $script = <<< JS
    
   $(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
        console.log("beforeInsert");
    });
    
    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        console.log("afterInsert");
    });
    
    $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });
    
    $(".dynamicform_wrapper").on("afterDelete", function(e) {
        console.log("Deleted item!");
    });
    
    $(".dynamicform_wrapper").on("limitReached", function(e, item) {
        alert("Limit reached");
    });
JS;
                $this->registerJs($script);
                ?>
