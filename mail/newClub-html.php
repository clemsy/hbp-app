<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $club app\models\Club */

$validationLink = Yii::$app->urlManager->createAbsoluteUrl(['clubs/'.$club->slug]);
?>
<div class="password-reset">
    <p>Hello,</p>

    <p>A new Bike Polo Club was just added to the website.</p>
    <p>Click the following link to review and validate this new entry.</p>

    <p><?= Html::a(Html::encode($validationLink), $validationLink) ?></p>
</div>
<p>

</p>
