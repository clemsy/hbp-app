<?php
/* @var $this yii\web\View */
/* @var $club app\models\Club */
$validationLink = Yii::$app->urlManager->createAbsoluteUrl(['clubs/'.$club->slug]);
?>
Hello,

A new Bike Polo Club was just added to the website.

Click the following link to review and validate this new entry.

<?=$validationLink;?>
